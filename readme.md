<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>
Gestion des dévis


## Framework utitlisé : Laravel

Le projet a été développé avec le framework PHP Laravel 

## Utilisation


> Veuillez au préalable installer **Composer** et **Git** 

Entrez dans l'invite de commande et positionnez-vous au dossier de votre choix où sera sauvé le projet

`git clone https://gitlab.com/ladinstar/arsgroup.git`

`cd arsgroup`

`cp .env.example .env`

Après cela allez modifier le contenu du fichier .env qui sera crée

DB_DATABASE=**arsgroup**

DB_USERNAME=**root**

DB_PASSWORD=**mot de passe**

Pour l'envoie des message de contact dans votre boite mail veillez remplir cette partie

MAIL_USERNAME=**nomdutilisateur@gmail.com**

MAIL_PASSWORD=**motdepasse**

Installez maintenant toutes les dépendance de Laravel

`composer install`

Après avoir fait tout ceci vous devez maintenant faire migrer votre base de données. Et n'oubliez pas que vous devez au préalable créer une base de données qui va accueillir tout ceci

`php artisan migrate:fresh`

Maintenant vous pouvez donc lancer le projet en tapant

`php artisan serve`

Merci !