<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

Class BaseModel extends Model{

    protected $guarded = [];

    use SoftDeletes;

}
