<?php

namespace App\Http\Controllers;

use App\Devis;
use App\Http\Requests\DevisRequest;
use Illuminate\Support\Facades\Redirect;
use MercurySeries\Flashy\Flashy;

class DevisController extends Controller
{
    /**
     * Affiches les dévis traités
     *
     * @return void
     */
    public function index(){
        $devis = Devis::where('active',1)->get();
        return view('devis.index',compact('devis'));
    }

    /**
     * Affiche les dévis non traités
     *
     * @return void
     */
    public function nontraite(){
        $devis = Devis::where('active',0)->get();
        return view('devis.nontraite',compact('devis'));
    }

    /**
     * Affiche la vu d'ajour de dévis
     *
     * @return void
     */
    public function add(){
        return view('devis.add');
    }

    /**
     * Permet de sauvegarder un dévis
     *
     * @param DevisRequest $request
     * @return void
     */
    public function store(DevisRequest $request){

        $logoName = time().'.'.$request->logo->getClientOriginalExtension();
        $request->logo->move(public_path('images/logo'), $logoName);

        $cahierName = time().'.'.$request->cahier->getClientOriginalExtension();
        $request->cahier->move(public_path('cahier'), $cahierName);

        $devis = Devis::create([
            'name' => $request->name,
            'email' => $request->email,
            'name' => $request->name,
            'project_name' => $request->project_name,
            'duration' => $request->duration,
            'begin' => $request->begin,
            'email' => $request->email,
            'logo' => $logoName,
            'budget' => $request->budget,
            'cahier' => $cahierName,
            'phone' => $request->phone,
            'description' => $request->description
        ]);

        if(!$devis){
            Flashy::error('Une erreur s\'est produite lors de l\'envoi de votre formulaire', route('home'));
        }
        else{
            Flashy::success('Merci de nous avoir contacter vous aurez de nos nouvelles lorsque votre demande sera traitée');
        }
        return Redirect::back();
    }

    /**
     * Permet de valider un devis
     *
     * @param [type] $id Identifiant du devis
     * @return void
     */
    public function valid($id){
        $devis = Devis::findOrFail($id);
        $devis->active = 1;
        $devis->save();

        if(!$devis){
            Flashy::error('Une erreur s\'est produite lors de la validation de votre devis', route('home'));
        }
        else{
            Flashy::success('Votre devis a été validé avec succès', route('home'));
        }
        return Redirect::back();
    }

    /**
     * Permet de lever la validation d'un devis
     *
     * @param [type] $id Identifiant du devis
     * @return void
     */
    public function unvalid($id){
        $devis = Devis::findOrFail($id);
        $devis->active = 0;
        $devis->save();

        if(!$devis){
            Flashy::error('Une erreur s\'est produite lors de la modification de votre devis', route('home'));
        }
        else{
            Flashy::success('Votre devis a été modifiée avec succès', route('home'));
        }
        return Redirect::back();
    }
}
