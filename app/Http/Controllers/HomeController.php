<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Affiche la page d'accueil du site
     *
     * @return void
     */
    public function index(){
        return view('home');
    }

    /**
     * Affiche les différentes réalisations de l'entreprise
     *
     * @return void
     */
    public function realisations(){
        return view('realisations');
    }
}
