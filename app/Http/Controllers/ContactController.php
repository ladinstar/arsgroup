<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use MercurySeries\Flashy\Flashy;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ContactController extends Controller
{
    /**
     * Enregistre les informations relatives au contact et notifie l'administrateur par mail
     *
     * @param ContactRequest $request
     * @return void
     */
    public function store(ContactRequest $request){
        $contact = Contact::create([
            'first_name' => $request->first_name,
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
        ]);

        Mail::send('partials.contactemail',
        array(
            'first_name' => $request->get('first_name'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'bodyMessage' => $request->get('message')
        ), function($message) use ($request)
        {
            $message->from($request->get('email'));
            $message->to(env("MAIL_USERNAME", "alladintroumba@gmail.com"), 'Admin')->subject($request->get('subject'));
        });

        Flashy::success('Merci de nous avoir contacter vous aurez de nos nouvelles lorsque votre demande sera traitée', route('home'));
        return Redirect::back();
    }
}
