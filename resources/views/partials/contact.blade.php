
    <div class="row" style="color:white;margin-top:10px;padding:250px 0px 70px 50px;background:url('https://arsgroupe.cm/wp-content/uploads/2019/08/bg-contact-1024x463.png');background-position: center;background-repeat: no-repeat;background-size: cover;">
        <div class="col-md-6" style="">
            <h1 style="font-weight: bold;">Nous contacter</h1>
            <br>
            <p>Tel: +237 690327402</p>
            <p>Email : alladintroumba@gmail.com</p>
            <p>Adresse : Rue du Cannard</p>
        </div>
        <div class="col-md-6" style="padding-right:50px">
            <div class="contact-form">
                <form action="{{ route('contact.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" placeholder="Prénom" name="first_name" class="form-control" id="inputFirstName" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" placeholder="Nom" name="name" class="form-control" id="inputLastName" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Adresse email" name="email" class="form-control" id="inputEmail" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Message" name="message" id="inputMessage" rows="5" required></textarea>
                    </div>
                    <div style="position:absolute;right:50px">
                        <input type="submit" class="btn btn-primary btn-lg home" value="Envoyer">
                    </div>

                </form>
            </div>
        </div>
    </div>
