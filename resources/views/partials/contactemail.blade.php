<h2>Hello Admin,</h2>
<p>Vous avez recu un email de : {{ $name }}</p>
<p>Les details sont ici:</p>
<p>Nom: {{ $name }}</p>
<p>Prénom: {{ $first_name }}</p>
<p>Email: {{ $email }}</p>
<p>Message: {{ $bodyMessage }}</p>
<p>Merci !</p>
