<style>
footer h5{
    font-weight: bold;
    font-size:25px;
    margin-bottom:15px;
}
</style>
<footer>
    <div class="row justify-content-center" style="margin:30px 0;">
        <img src="{{ asset('img/logo.png') }}" height="20" alt="">
    </div>
    <div class="row" style="padding:1em 2em;text-align:center">
        <div class="col-md-3">
            <h5>A propos</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Présentation</a></li>
                <li><a class="text-muted" href="#">Equipe</a></li>
                <li><a class="text-muted" href="#">Historique</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h5>Projets</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Projets en cours</a></li>
                <li><a class="text-muted" href="#">Projets en attente</a></li>
                <li><a class="text-muted" href="#">Projets réalisés</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h5>Projets</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Projets en cours</a></li>
                <li><a class="text-muted" href="#">Projets en attente</a></li>
                <li><a class="text-muted" href="#">Projets réalisés</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-md">
            <h5>Projets</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Projets en cours</a></li>
                <li><a class="text-muted" href="#">Projets en attente</a></li>
                <li><a class="text-muted" href="#">Projets réalisés</a></li>
            </ul>
        </div>
    </div>
    <div class="row justify-content-center">
        <p>
            <h5>ARS-GROUP 2019</h5>
        </p>
    </div>
</footer>
