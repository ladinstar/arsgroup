<style>
    .but .btn.btn-primary.btn-lg{
        padding:15px 40px;
        background-color: transparent;
        border:1px solid #dc0303;
        transition: background ease-in 0.2s;
        color:black;
    }
    .but .btn.btn-primary.btn-lg:hover,.btn.btn-primary.btn-lg.active{
        background-color: #dc0303;
        border:1px solid transparent;
        box-shadow: none;
        transition: background ease-in 0.2s;
        color:white;
    }
</style>
<div class="container but" style="padding:40px 0px">
    <div class="row">
        <div class="col-sm-4">
            <p style="text-align:left">
                <a href="{{ route('devis.add') }}" class="btn btn-primary btn-lg {{ setActive('devis.add') }}">
                    <i class="fa fa-plus"></i> Nouveau devis
                </a>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align:center">
                <a href="{{ route('devis.nontraite') }}" class="btn btn-primary btn-lg {{ setActive('devis.nontraite') }}">
                    <i class="fa fa-trash"></i> Dévis non traités
                </a>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align:right">
                <a href="{{ route('devis.index') }}" class="btn btn-primary btn-lg {{ setActive('devis.index') }}">
                    <i class="fa fa-check"></i> Dévis traités
                </a>
            </p>
        </div>
    </div>
</div>
