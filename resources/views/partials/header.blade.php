<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('img/logo.png') }}" alt="" width="300px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent" style="position:absolute;right:10px">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ setActive('home') }}">
                    <a class="nav-link" href="{{ route('home') }}">Acceuil</a>
                </li>
                <li class="nav-item {{ setActive('devis') }}">
                    <a class="nav-link" href="{{ route('devis.index') }}">Mes devis</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mes projets</a>
                </li>
                <li class="nav-item {{ setActive('realisations') }}">
                    <a class="nav-link" href="{{ route('realisations') }}">Nos réalisations</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
