@extends('layouts.template')

@section('title','Les différentes realisations de ARG-GROUPE')

@section('css')
<style>
    .row.featurette h2{
        color:red;
        margin-bottom: 25px;
    }
    .row.featurette .col-md-6:last-of-type{
        border-left: 10px solid silver;
    }
    img.bd-placeholder-img{
        border-radius: 50%;
        vertical-align: middle;
    }
    div.col-md-6.img{
        display: table-cell;
        vertical-align: middle;
    }
    .inner {
        padding:40px 30px;
        border-radius:50%;
        background: #dc0303;
        position: absolute;
        top: 50%;
        left: 50%;
        z-index:99999;
        text-align:center;
        font-size: 25px;
        font-weight: bold;
        color:white;
        transform: translate(-46%, -46%);
    }
    .featurette hr{
        margin:3em 0;
        display:inline-block;
        border:3px solid #dc0303;
        background: #dc0303;
        text-align:center;
        width:20em;
    }
</style>
@endsection
@section('content')
<div class="row" style="padding:15em 0;background-image:url('https://arsgroupe.cm/wp-content/uploads/2019/08/background-header-1024x411.png');background-position: center;background-repeat: no-repeat;background-size: cover;">
    <div class="col-sm-12" style="text-align:center;color:white">
        <h1>NOS REALISATIONS</h1>
        <hr style="width:20%;height:5px;background:white">
    </div>
</div>
<div class="container" style="text-align:center">
    <div class="row featurette">
        <div class="col-md-12">
            <div class="inner">2018</div>
        </div>
        <div class="col-md-6">
            <h2 class="featurette-heading">Flamingo</h2>
            <p class="lead text-justify">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
            <p style="text-align:center"><hr></p>
        </div>
        <div class="col-md-6 img">
            <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="300" height="300" src="https://arsgroupe.cm/wp-content/uploads/2019/08/images-1.png">
        </div>
    </div>
    <div class="row featurette">
        <div class="col-md-12">
            <div class="inner">2017</div>
        </div>
        <div class="col-md-6 order-md-1 img">
            <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="300" height="300" src="https://arsgroupe.cm/wp-content/uploads/2019/08/images-1.jpeg">
        </div>
        <div class="col-md-6 order-md-2">
            <h2 class="featurette-heading">Roma</h2>
            <p class="lead text-justify">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
            <p style="text-align:center"><hr></p>
        </div>
    </div>
    <div class="row featurette">
        <div class="col-md-12">
            <div class="inner" style="transform: translate(-46%, 300%);">2015</div>
        </div>
        <div class="col-md-6">
            <h2 class="featurette-heading">ARC</h2>
            <p class="lead text-justify">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
            <p style="text-align:center"><hr></p>
        </div>
        <div class="col-md-6 img">
            <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="300" height="300" src="https://arsgroupe.cm/wp-content/uploads/2019/08/images-2.jpeg">
        </div>
    </div>
</div>
<!--================ Start Clients Logo Area =================-->
<section class="clients_logo_area section_gap">
    <div class="container">
        <div class="clients_slider row justify-content-center align-items-center">
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/1.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/2.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/3.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/4.png') }}" alt="">
            </div>
        </div>
    </div>
</section>
@endsection
