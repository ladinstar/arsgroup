@extends('layouts.template')

@section('title','Ajout d\'un devis - ARS-GROUPE')

@section('css')
<style>

    input,
    textarea {
        margin: 20px 0px;
    }
</style>
@endsection
@section('content')
<div class="row"
    style="padding:15em 0;background-image:url('https://arsgroupe.cm/wp-content/uploads/2019/08/background-header-1024x411.png');background-position: center;background-repeat: no-repeat;background-size: cover;">
    <div class="col-sm-12" style="text-align:center;color:white">
        <h1>MES DEVIS</h1>
        <hr style="width:20%;height:5px;background:white">
    </div>
</div>
@include('partials.devis_buttons')
<div class="row"
    style="background:url('https://arsgroupe.cm/wp-content/uploads/2019/08/bg-contact-1024x463.png');background-position: center;background-repeat: no-repeat;background-size: cover;">
    <form action="{{ route('devis.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container" style="margin-bottom:50px;">
            <div class="row" style="color:white;margin-top:10px;padding:200px 30px 80px 30px;">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Ooops!</strong> Veuillez vérifier les informations de votre formulaire.
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-md-6">
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Nom du contact" name="name" class="form-control"
                                        id="inputEmail" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Nom du Projet" name="project_name"
                                        class="form-control" id="inputEmail" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Durée" name="duration" class="form-control"
                                        id="inputFirstName" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Debut" name="begin" class="form-control"
                                        id="inputLastName" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" placeholder="Email" name="email" class="form-control" id="inputEmail"
                                required>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="file" name="logo" class="form-control-file"
                                        id="exampleFormControlFile1">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Budget" name="budget" class="form-control"
                                        id="inputFirstName" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="file" name="cahier" class="form-control-file"
                                        id="exampleFormControlFile1">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="tel" placeholder="Téléphone" name="phone" class="form-control"
                                        id="inputEmail" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Description du projet" name="description"
                            id="inputMessage" rows="5" required></textarea>
                    </div>
                    <div style="position:absolute;right:50px">
                        <input type="submit" class="btn btn-primary btn-lg home" value="Envoyer">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
