@extends('layouts.template')

@section('title','Les dévis non traités - ARS-GROUPE')

@section('css')
<style>
.row.devis{
    box-shadow: -1px 1px 2px black;
    margin-top:6em;
}
.row.devis .col-md-6{
    padding-right: 0;
    padding-left: 0;
}
ul.outline-round {
  font-family: "Roboto";
  font-size: 25px;
  line-height: 1.5em;
  margin: 5px 0 15px;
  padding: 0;
}
ul.outline-round li {
  list-style: none;
  position: relative;
  padding: 0 0 0 20px;
}
ul.outline-round li::before {
  content: "";
  position: absolute;
  left: 20%;
  top: 4px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  border: 5px solid #dc0303;
}
.row.devis h2{
    color:#dc0303;
    font-size:3em;
}
.inner {
    padding:40px 30px;
    border-radius:100%;
    background: #dc0303;
    position: absolute;
    top:5px;
    right:5px;
    z-index:99999;
    text-align:center;
    font-size: 25px;
    font-weight: bold;
    color:white;
    transform: translate(50%, -50%);
}
.inner-img {
    height:7em;
    width: 7em;
    border-radius:100%;
    background: #dc0303;
    position: absolute;
    top:50%;
    right:10%;
    z-index:999999;
    transform: translate(100%, -50%);
}
</style>
@endsection
@section('content')
<div class="row" style="padding:15em 0;background-image:url('https://arsgroupe.cm/wp-content/uploads/2019/08/background-header-1024x411.png');background-position: center;background-repeat: no-repeat;background-size: cover;">
    <div class="col-sm-12" style="text-align:center;color:white">
        <h1>MES DEVIS</h1>
        <hr style="width:20%;height:5px;background:white">
    </div>
</div>
@include('partials.devis_buttons')
<div class="container" style="text-align:center">
    @forelse ($devis as $devi)
        @if ($loop->iteration % 2 != 0)
            <div class="row devis align-items-center">
                <div class="col-md-6 ">
                    <h2 class="">{{ $devi->project_name }}</h2>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="outline-round">
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="outline-round">
                                <li>Service 1</li>
                                <li>Service 2</li>
                                <li>Service 3</li>
                                <li>Service 4</li>
                                <li>Service 5</li>
                            </ul>
                        </div>
                        <div class="container but">
                            <div class="col-sm-12">
                                <p style="text-align:center">
                                    <a href="{{ route('devis.valid',$devi->id) }}" class="btn btn-primary btn-lg active">
                                        <i class="fa fa-check"></i> VALIDER
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                <img class="inner-img" src="{{ asset('images/logo/'.$devi->logo) }}" alt="">
                </div>
                <div class="col-md-6">
                    <img src="https://arsgroupe.cm/wp-content/uploads/2019/08/te%CC%81le%CC%81chargement.jpeg" style="width:100%">
                    <div class="inner">{{ $devi->budget }} $</div>
                </div>
            </div>
        @else
            <div class="row devis align-items-center">
                <div class="col-md-6">
                    <img src="https://arsgroupe.cm/wp-content/uploads/2019/08/te%CC%81le%CC%81chargement.jpeg" style="width:100%">
                    <img class="inner-img" src="{{ asset('images/logo/'.$devi->logo) }}" alt="">
                </div>
                <div class="col-md-6 ">
                    <h2 class="">{{ $devi->project_name }}</h2>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="outline-round">
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                                <li>Fonction 1</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="outline-round">
                                <li>Service 1</li>
                                <li>Service 2</li>
                                <li>Service 3</li>
                                <li>Service 4</li>
                                <li>Service 5</li>
                            </ul>
                        </div>
                        <div class="container but">
                            <div class="col-sm-12">
                                <p style="text-align:center">
                                    <a href="{{ route('devis.valid',$devi->id) }}" class="btn btn-primary btn-lg active">
                                        <i class="fa fa-check"></i> VALIDER
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="inner">{{ $devi->budget }} $</div>
                </div>
            </div>
        @endif
    @empty
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-primary" role="alert">
                Ooops ! Il n'y as aucun devis non traité pour le moment
            </div>
        </div>
    </div>
    @endforelse
</div>
@endsection
