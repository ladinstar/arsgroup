@extends('layouts.template')

@section('title','Bienvenue sur la page d \'acceuil de ARG-GROUPE')

@section('style')
<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>
@endsection
@section('content')
<div id="myCarousel" class="carousel slide pointer-event" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class=""></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item">
            <img class="bd-placeholder-img"
                src="https://arsgroupe.cm/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-14-at-08-1024x568.png" alt="">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Faites vos devis chez vous</h1>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                    <p><a class="btn btn-primary btn-lg home" href="#" role="button">Commencer</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="bd-placeholder-img"
                src="https://arsgroupe.cm/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-14-at-08-1024x568.png" alt="">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Faites vos devis chez vous</h1>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                    <p><a class="btn btn-primary btn-lg home" href="#" role="button">Commencer</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item active">
            <img class="bd-placeholder-img"
                src="https://arsgroupe.cm/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-14-at-08-1024x568.png" alt="">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Faites vos devis chez vous</h1>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                    <p><a class="btn btn-primary btn-lg home" href="#" role="button">Commencer</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <img src="{{ asset('img/left.png') }}" class="home-img" alt="">
    </div>
    <div class="col-sm-6" style="padding-top:12%">
        <div class="row align-items-center justify-content-center" style="margin-top:10px">
            <i class="fa fa-search"
                style="color:white;font-size:60px;padding:30px;border-radius:50%;background-color:#FB0000;"></i>
        </div>
        <div class="row align-items-center justify-content-center" style="margin-top:10px">
            <h2 style="color:#FB0000;">Solution adpatée</h2>
        </div>
        <p style="text-align:justify;font-size:20px;margin-top:30px">Le <strong>Lorem ipsum</strong> dolor sit amet
            consectetur adipisicing elit. Ipsam qui dignissimos asperiores ullam, pariatur autem excepturi laboriosam,
            beatae error quo, harum obcaecati nostrum ipsa doloremque vel unde hic in similique.</p>
        <p style="text-align:right">
            <a href="#" style="color:#FB0000;">Lire la suite <i class="fa fa-long-arrow-right"></i></a>
        </p>
    </div>
</div>

<div class="row">
    <div class="col-sm-6" style="padding-top:12%">
        <div class="row align-items-center justify-content-center" style="margin-top:10px">
            <i class="fa fa-search"
                style="color:white;font-size:60px;padding:30px;border-radius:50%;background-color:#6b6a6a;"></i>
        </div>
        <div class="row align-items-center justify-content-center" style="margin-top:10px">
            <h2 style="color:#FB0000;">Solution adpatée</h2>
        </div>
        <p style="text-align:justify;font-size:20px;margin-top:30px">Le <strong>Lorem ipsum</strong> dolor sit amet
            consectetur adipisicing elit. Ipsam qui dignissimos asperiores ullam, pariatur autem excepturi laboriosam,
            beatae error quo, harum obcaecati nostrum ipsa doloremque vel unde hic in similique.</p>
        <p style="text-align:right">
            <a href="#" style="color:#FB0000;">Lire la suite <i class="fa fa-long-arrow-right"></i></a>
        </p>
    </div>
    <div class="col-sm-6">
        <img src="{{ asset('img/right.png') }}" class="home-img" alt="">
    </div>
</div>
<div class="row" style="margin:50px 0;">
    <div class="col-sm-12" style="text-align:center">
        <p class="center-align"><h2>NOS REALISATIONS</h2></p>
    </div>
</div>
<style>
    div.col-sm-3.realisation h1,div.col-sm-1.realisation h1{
        color : #FB0000;
        font-weight: bold;
    }
</style>
<div class="row" style="text-align:center;padding:0px 10em;">
    <div class="col-sm-3 realisation">
        <h1>465</h1>
        <p>CMS</p>
    </div>
    <div class="col-sm-1 realisation">
        <h1>.</h1>
    </div>
    <div class="col-sm-3 realisation">
        <h1>189</h1>
        <p>FRAMEWORK</p>
    </div>
    <div class="col-sm-1 realisation">
        <h1>.</h1>
    </div>
    <div class="col-sm-3 realisation">
        <h1>273</h1>
        <p>MOBILE</p>
    </div>
</div>
<!--================ Start Clients Logo Area =================-->
<section class="clients_logo_area section_gap">
    <div class="container">
        <div class="clients_slider row justify-content-center align-items-center">
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/1.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/2.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/3.png') }}" alt="">
            </div>
            <div class="item col-md-3">
                <img src="{{ asset('img/partner/4.png') }}" alt="">
            </div>
        </div>
    </div>
</section>
@endsection
